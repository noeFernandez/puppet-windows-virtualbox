# Class: virtualbox_windows
#
# This module manages virtualbox_windows
#
# Parameters: none
#
# Actions:
#
# Requires: see Modulefile
#
# Sample Usage:
#
class virtualbox_windows ($version = $::virtualbox_windows::params::version, $build = $::virtualbox_windows::params::build) inherits virtualbox_windows::params {
  $url = "http://download.virtualbox.org/virtualbox/${version}/VirtualBox-${version}-${build}-Win.exe"
  $destination = "C:\\Windows\\Temp\\VirtualBox-${version}-${build}-Win.exe"


  case $::operatingsystem{
    "windows" : {
      notify{"Estoy en windows":}
    }

    default : {
      notify{"No se dxdxd":}
    }
  }

  exec { "Download Oracle VM VirtualBox ${version}":
    command  =>  template('virtualbox_windows/download.ps1'),
    logoutput => true,
    onlyif => template('virtualbox_windows/checkfileexists.ps1'),
    provider => powershell,
  } ->
  package { "Oracle VM VirtualBox ${version}" :
    ensure          => installed,
    source          => $destination,
    install_options => ["/silent"],
  }

}
