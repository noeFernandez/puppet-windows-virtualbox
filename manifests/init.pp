# Class: virtualbox_windows
#
# This module manages virtualbox in windows
#
# Parameters: version = virtualbox verison
#             build   = virtualbox build
#
# Actions:
#
# Requires: puppetlabs-powershell
#
# Sample Usage: Just write class{'virtualbox_windows':}
#
class virtualbox_windows (
  $version = $::virtualbox_windows::params::version,
  $build   = $::virtualbox_windows::params::build,
  $install_virtualbox_extensions = true) inherits virtualbox_windows::params {
  $url = "http://download.virtualbox.org/virtualbox/${version}/VirtualBox-${version}-${build}-Win.exe"
  $destination = "C:\\Windows\\Temp\\VirtualBox-${version}-${build}-Win.exe"

  case $::operatingsystem {
    "windows" : {
      exec { "Download Oracle VM VirtualBox ${version}":
        command   => template('virtualbox_windows/download.ps1'),
        logoutput => true,
        onlyif    => template('virtualbox_windows/checkfileexists.ps1'),
        provider  => powershell,
      } ->
      package { "Oracle VM VirtualBox ${version}":
        ensure          => installed,
        source          => $destination,
        install_options => ["/silent"],
      }

      $url = "http://download.virtualbox.org/virtualbox/4.3.24/Oracle_VM_VirtualBox_Extension_Pack-4.3.24-98716.vbox-extpack"
      $destination = "C:\\Windows\\Temp\\Oracle_VM_VirtualBox_Extension_Pack-4.3.24-98716.vbox-extpack"

      if ($install_virtualbox_extensions == true) {
        exec { "Download VirtualBox extensions":
          command   => template('virtualbox_windows/download.ps1'),
          logoutput => true,
          onlyif    => template('virtualbox_windows/checkfileexists.ps1'),
          provider  => powershell,
        }

        if ($::architecture == "x86") {
          $vboxmanage_dir = "C:\\progra~2\\Oracle\\VirtualBox\\VBoxManage.exe"
        } else {
          $vboxmanage_dir = "C:\\progra~1\\Oracle\\VirtualBox\\VBoxManage.exe"
        }

        exec { "Installing virtualbox extensions": command => "${vboxmanage_dir} extpack install ${destination}", }

      }
    }

    default   : {
      fail("This operative system [${operatingsystem}] is not (yet) supported")
    }
  }

}
